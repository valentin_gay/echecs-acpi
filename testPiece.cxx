/** 
 * Programme test de Piece
 *
 * @file testPiece.cxx
 */

// Utile pour l'affichage
#include <iostream>
#include "Piece.h"
#include "Joueur.h"
#include "Echiquier.h"
#include <stdio.h>

// Pour utiliser les flux de iostream sans mettre "std::" tout le temps.
using namespace std;

bool compare(Piece &p1, Piece &p2)
{
  return (p1.x()==p2.x()) && (p1.y()==p2.y());
}

/**
 * Programme principal
 */
int main( int argc, char** argv )
{
  // instancie un objet p1 de type Piece
  Piece p1;
  // p1 est une piece blanche de coordonnees (3,3)
  p1.init( 3, 3, true );
  p1.affiche();

  Piece p2( 4, 4, false );
  p2.affiche();

  //  Piece tbl[4];

  /*if (compare(p1,p2))
    cout << "memes positions" << endl;
  else
    cout << "differentes positions" << endl; */

  Piece p3 = p2; // Piece p3(p2);
  p3=p2=p1;
  
  
  Joueur jb(true);
  Joueur jn(false);

 
  
  Echiquier e;
  
  jb.placerPieces(e);
  jn.placerPieces(e);
  
  e.affiche();
  
  printf("\n");
  
  
  // les objets definis dans cette fonction sont automatiquement d�truits.
  // Ex : p1
}
