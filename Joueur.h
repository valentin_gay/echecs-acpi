#if !defined Joueur_h
#define Joueur_h

#include "Echiquier.h"
#include "Piece.h"

class Joueur
{
 private:
  Piece m_pieces[16];

 public:
  Joueur();
  ~Joueur();
  Joueur(bool white);
  void affiche();
  bool placerPieces(Echiquier &e);
};

#endif // Joueur_h
