#include <iostream>
#include "Joueur.h"

using namespace std;

Joueur::Joueur()
{
  cout << "Un Joueur construit par defaut" << endl;
}

Joueur::~Joueur()
{
  cout << "Un Joueur detruit" << endl;
}

Joueur::Joueur(bool white)
{
  int n = 0;
  
  int y = white ? 1 : 8;
  for ( int x = 1; x <= 8; ++x )
    {
      m_pieces[ n ].init( x, y, white );
      n=n+1; // n++;
    }

  y = white ? 2 : 7;
  for ( int x = 1; x <= 8; ++x )
    m_pieces[ n++ ].init( x, y, white );

  cout << "Un Joueur specialise construit" << endl;
}

void
Joueur::affiche()
{
  for (int i=0;i<16;i++)
    m_pieces[i].affiche();
}

bool
Joueur::placerPieces(Echiquier &e)
{
    bool ok=true;
    for (int i=0;i<16;i++) 
    {
        if(!e.placer(&m_pieces[i])) return false;
    }
    
    return true;
}
