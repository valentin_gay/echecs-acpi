PROG=testPiece

SRC_CXX=Piece.cxx Joueur.cxx Echiquier-etd.cxx

OBJ=${SRC_CXX:.cxx=.o}

$(PROG): $(OBJ) $(PROG).o
	g++ $(OBJ) $(PROG).o -o $(PROG)

%.o: %.cxx
	g++ -c $<

clean:
	@rm -f $(OBJ) $(PROG).o $(PROG) *~

